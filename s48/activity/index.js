// DOCUMENTATION: 
/*
1. https://www.javascripttutorial.net/javascript-dom/

2. https://www.w3schools.com/jsref/event_preventdefault.asp

3. https://developer.mozilla.org/en-US/docs/Web/API/Element/removeAttribute

4. https://developer.mozilla.org/en-US/docs/Web/API/Element/setAttribute
*/

// JSON PLACEHOLDER
// https://jsonplaceholder.typicode.com/posts
// fetch

// fetch - gets data
// then - what will happen next


// "fetch()" method returns a promise that resolves to a Response object

fetch('https://jsonplaceholder.typicode.com/posts')
.then((response) => response.json())
//.then((json) => console.log(json));
.then((data) => showPosts(data))
.then((data) => deletePost(data));


/*fetch('https://jsonplaceholder.typicode.com/posts/')
.then((response) => response.json())
.then((data) => deletePost(data));*/


// ADD POST DATA
// ADD POST DATA
// ADD POST DATA



// (e) = event
document.querySelector('#form-add-post').addEventListener("submit", (e) => {


// preventDefault : https://www.w3schools.com/jsref/event_preventdefault.asp

	// Prevents the page from reloading
	// Prevents the default behavior of event
	// submit - page reloading

	e.preventDefault();

	fetch('https://jsonplaceholder.typicode.com/posts',
		{
			method: 'POST',
			headers: 
			{
				'Content-Type': 'application/json; charset=UTF-8'
			},
			body: JSON.stringify({
				title: document.querySelector('#txt-title').value,
				body: document.querySelector('#txt-body').value,
				userId: 1
			})
		})
	.then((response) => response.json())
	.then((data) => {
		console.log(data);
		alert('Successfully added!');

		// clear text fields
		document.querySelector('#txt-title').value = null;

		document.querySelector('#txt-body').value = null;
	});
});



// RETRIEVE POSTS
// RETRIEVE POSTS
// RETRIEVE POSTS



const showPosts = (posts) => {
    let postEntries = "";

    posts.forEach((post) => {
 
        postEntries += `
            <div id ="post-${post.id}">
                <h3 id="post-title-${post.id}">${post.title}</h3>
                <p id="post-body-${post.id}">${post.body}</p>
                <button onClick="editPost('${post.id}')">Edit</button>
                <button onClick="deletePost('${post.id}')">Delete</button>
            </div>
        `
    });

    //to check what is stored in the properties of variables 
    // console.log(postEntries)

    document.querySelector("#div-post-entries").innerHTML = postEntries;
};


// EDIT POST
// EDIT POST
// EDIT POST


const editPost = (id) => {

	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector("#txt-edit-id").value = id;
	document.querySelector("#txt-edit-title").value = title;
	document.querySelector("#txt-edit-body").value = body;


// removeAttribute - https://developer.mozilla.org/en-US/docs/Web/API/Element/removeAttribute
	document.querySelector("#btn-submit-update").removeAttribute('disabled');
};



// UPDATE POST

document.querySelector('#form-edit-post').addEventListener("submit", (e) => {

	e.preventDefault();

	fetch('https://jsonplaceholder.typicode.com/posts/1',
		{
			method: 'PUT',
			headers: 
			{
				'Content-Type': 'application/json; charset=UTF-8'
			},
			body: JSON.stringify({
				id: document.querySelector('#txt-edit-id'),
				title: document.querySelector('#txt-edit-title').value,
				body: document.querySelector('#txt-edit-body').value,
				userId: 1
			})
		})
	.then((response) => response.json())
	.then((data) => {
		console.log(data);
		alert('Successfully updated!');

		// clear text fields
		document.querySelector('#txt-edit-id').value = null;
		document.querySelector('#txt-edit-title').value = null;
		document.querySelector('#txt-edit-body').value = null;

// setAttribute - https://developer.mozilla.org/en-US/docs/Web/API/Element/setAttribute
		document.querySelector("#btn-submit-update").setAttribute('disabled', true);

	});
});



// ACTIVITY PROPER
// DELETE POST

const deletePost = (id) => {

	// tried and tested
	// achieved intended result

	let removePost = document.querySelector(`#post-${id}`);
	
	fetch('https://jsonplaceholder.typicode.com/posts/1',
		{
		method: 'DELETE',
		})
	.then((result) => 
	{
		//document.querySelector(`#post-${id}`).remove();
		removePost.remove();
		alert(`Post #${id} was deleted.`);
		console.log(`Post #${id} was deleted.`);
		
	});

};
