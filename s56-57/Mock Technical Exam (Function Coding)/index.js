// PASSED 
function countLetter(letter, sentence) {
    let result = 0;

    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.


    if (letter.length-1 > 0 ) // if letter is more than 1 
    {
        return undefined
    }
    else
    {
        let letter_count = 0;

        for (let position=0; position<sentence.length; position++)
        {
            if (sentence.charAt(position) == letter)
            {
                letter_count +=1;
            }
        }

        return letter_count
    }

}


// PASSED 
function isIsogram(text) {
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.

    givenText = text.toLowerCase();

    let sortedLetters = givenText.split(''); // delete spaces
 
    sortedLetters.sort(); // sort alphabetically A-Z

    for (let ctr=0; ctr<givenText.length-1; ctr++)
    {
        if (givenText[ctr] == givenText[ctr+1]) // compares the current and next letter
            return false; //not isogram (with repeating letters)
    }
    return true; // is isogram (without repeating letters)
}


//PASSED 
function purchase(age, price) {
    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.

    if (age <= 12)
    {
        return undefined;
    }
    else if (age >= 13 && age <=21)
    {

        return (price *.8).toFixed(2);
    }
    else if (age >=22 && age <=64)
    {
        return price.toFixed(2);
    }
    else
    {
        return (price *.8).toFixed(2);
    }

  
}
/*console.log('Returns undefined for students aged below 13.')
console.log(purchase(12, 109.4356));


console.log('Returns discounted price (rounded off) for students aged 13 to 21.')
console.log(purchase(15, 109.4356));

console.log('Returns discounted price (rounded off) for senior citizens.')
console.log(purchase(72, 109.4356));

console.log('Returns price (rounded off) for people aged 22 to 64.')
console.log(purchase(34, 109.4356));*/


// PASSED
function findHotCategories(items) {
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.

function uniqueCategory(hotCategories) 
{
    return hotCategories.filter((item, index) => hotCategories.indexOf(item) === index);
}


let hotCat = [];

for(let i = 0; i < items.length; i++) {
    if (items[i].stocks === 0) {
        hotCat.push(items[i].category);
    }
}


return uniqueCategory(hotCat);

}

//PASSED 
function findFlyingVoters(candidateA, candidateB) {
    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
    
    //comparing of arrays
   let flyingVotersArray = [];

    let cA = candidateA, cB = candidateB;

    for (let ctr1 = 0; ctr1 < cA.length; ctr1++)
    {
        for (let ctr2 = 0; ctr2 <cB.length; ctr2++)
        {
            if (cA[ctr1] == cB[ctr2])
            {
                flyingVotersArray.push(cA[ctr1])
            }
        }
    }
    return flyingVotersArray;
}


module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};

















//reference for future use

  /*  let arr = [
      {"id":1,"name":"admin","password":"admin","role":"Admin"},
      {"id":2,"name":"user","password":"user","role":"User"},
      {"id":3,"name":"superadmin","password":"superadmin","role":"superadmin"}
    ];
*/
/*    arr.forEach((item, i) => {
       for(let key in item) {
           console.log(`${key}: ${item[key]}`);
       }
    });
*/

/*arr.forEach((item, i) => {
   for(let key in item) {
      if (key === 'name' || key === "password") {
          console.log(`${key}: ${item[key]}`);
      }
   }
});*/