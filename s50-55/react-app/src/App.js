import AppNavbar from './components/AppNavbar';
import CourseView from './components/CourseView';
//import Banner from './components/Banner';

import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';

import {BrowserRouter as Router, Routes, Route}  from 'react-router-dom'

import './App.css';

import {Container} from 'react-bootstrap';

import {UserProvider} from './UserContext';

import {useState, useEffect} from 'react';

function App() {

    //const [user, setUser] = useState({email:localStorage.getItem('email')});
    const [user, setUser] = useState({
      id: null,
      isAdmin: null
    });

    // used to heck if the use information is properly stored upon login in the localstorage and cleared upon logout
   useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then( data => {

      // user us logged in
      if(typeof data._id !== "undefined") {
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
        // user is logged out
      } else {
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })
  }, []);

    const unsetUser = () => 
    {
      localStorage.clear();
    }

  return (
    // <> </> fragment eme
    <>
      <UserProvider value={{user, setUser, unsetUser}}> 
        <Router>
          <AppNavbar />
          <Container >
            <Routes>
              <Route path="/" element={<Home />} />

              <Route path="/courses" element={<Courses />} />

              <Route path="/courses/:courseId" element={<CourseView />} />

              <Route path="/register" element={<Register />} /> 

              <Route path="/login" element={<Login />} />

              <Route path="/logout" element={<Logout />} />

              <Route path="/*" element={<Error/>} />
            </Routes>
          </Container>
        </Router>
      </UserProvider>
    </>
  );
}

export default App;
